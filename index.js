function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    if (letter.length === 1){

        function letterCount(e){
            return e === letter
        }
    
        const sentenceLower = sentence.toLowerCase().split("")

        result = sentenceLower.filter(letterCount).length

        return result
    } 

}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
const textLower = text.toLowerCase().split("")
    let checkedLetters = []

    for (let i = 0; i < textLower.length; i++) {
        if (checkedLetters.includes(textLower[i])){
            return false
            break;
        } else {
            checkedLetters.push(textLower[i])
            continue
        }
    }

    if (textLower.length === checkedLetters.length){
        return true
    }

    
}

function purchase(age, price) {
    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    
     const discounted = price * 0.8
    const rounded = price.toFixed(2)
    const disRounded = discounted.toFixed(2)

    if ((age > 12 && age < 22) || (age > 64)) {

        return disRounded.toString()

    } else if (age > 21 && age < 65) {

        return rounded.toString()

    }

}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

let hotCategory = []

    items.forEach(checkHot)

    function checkHot(e){
        if (e.stocks === 0) {
            if (!hotCategory.includes(e.category)){
                hotCategory.push(e.category)
            }
        }
    }

    return hotCategory

}

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    let duplicates = []

    candidateA.forEach(function(candidate){
        console.log(candidate)

        if (candidateB.includes(candidate)){
            duplicates.push(candidate)
        }

    })

    return duplicates

}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};